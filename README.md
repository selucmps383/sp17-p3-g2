## Overview

In this phase, we will be building an Angular 2 SPA utilizing the League of Legends API. This SPA should allow uses to search summoners by name, view summoner match history, and match detail. The match detail should have images for characters and items.

## Installation and Technology

In this phase, we will be using Node.JS, Angular 2, Angular CLI, and Visual Studio Code. Once each is installed, you can generate a starter project through Angular CLI.

## API Reference

We will be using the League of Legends API through Riot Games.
Link: https://developer.riotgames.com/
Summoner Name: (/api/lol/{region}/v1.4/summoner/by-name/{summmonerNames}?api_key={yourApiKey}
Match History: (/api/lol/{region}/v2.2/matchlist/by-summoner/{summonerId})

## Important Information

There will be a code freeze on Monday, March 13th at 11:59 PM. Presentations are limited to 10 minutes.