import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { SummonerComponent } from './summoner/summoner.component';
import { SummonerService } from './summoner/summoner.service';
import { DataHandlerService } from './summoner/data-handler.service';

@NgModule({
  declarations: [
    AppComponent,
    SummonerComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [SummonerService, DataHandlerService],
  bootstrap: [AppComponent]
})
export class AppModule { }
