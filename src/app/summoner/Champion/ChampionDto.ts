import { Image } from './Image';
export interface ChampionDto
{
    id: number;
    key: string;
    name: string;
    title: string;
    image: Image;
}