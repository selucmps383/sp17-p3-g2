export interface Image
{
    full: string;
    sprite: string;
    group: string;
    x: string;
    y: string;
    w: string;
    h: string;
}