import { PlayerDto } from './PlayerDto';
import { RawStatsDto } from './RawStatsDto';
export interface GameDto
{
    championId: number;
    createDate: number;
    fellowPlayers: PlayerDto[];
    gameId: number;
    gameMode: string;
    gameType: string;
    invalid: boolean;
    ipEarned: number;
    level: number;
    mapId: number;
    spell1: number;
    spell2: number;
    stats: RawStatsDto;
    subType: string;
    teamId: number;

}