export interface PlayerDto
{
    championId: number;
    summonerId: number;
    teamId: number;
}