import { GameDto } from './GameDto';
export interface RecentGamesDto
{
    games: GameDto[];
    summonerId: number;
}