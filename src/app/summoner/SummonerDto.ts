export interface SummonerDto
{
    id: number;
    name: string;
    profileIconId: number;
    revisionDate: number;
    summonerLevel: number;
}