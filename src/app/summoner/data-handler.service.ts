import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import { Player } from './player';
import { RecentGames } from './recentGames';
import { Observable } from 'rxjs/Rx';
import { DataHandler } from './data-handler';
import 'rxjs/add/operator/map';

@Injectable()
export class DataHandlerService {
    data: DataHandler = {
        player: {
            id: undefined,
            name: '',
            profileIconId: undefined,
            revisionDate: undefined,
            summonerLevel: undefined
        },
        recentGames: {
            championId: undefined,
            createDate: undefined,
            fellowPlayers: [],
            gameId: undefined,
            gameMode: '',
            gameType: '',
            invalid: false,
            ipEarned: undefined,
            level: undefined,
            mapId: undefined,
            spell1: undefined,
            spell2: undefined,
            stats: [],
            subType: '',
            teamId: undefined
        },
        matchHistory: {
            endIndex: undefined,
            matches: [],
            startIndex: undefined,
            totalGames: undefined
        },
        
    };
}