export interface DataHandler {
    player: {
        id: number;
        name: string;
        profileIconId: number;
        revisionDate: number;
        summonerLevel: number;
    },
    recentGames: {
        championId: number;
        createDate: number;
        fellowPlayers: any[];
        gameId: number;
        gameMode: string;
        gameType: string;
        invalid: boolean;
        ipEarned: number;
        level: number;
        mapId: number;
        spell1: number;
        spell2: number;
        stats: any[];
        subType: string;
        teamId: number;
    },
    matchHistory: {
        endIndex: number;
        matches: any[];
        startIndex: number;
        totalGames: number
    }
    
}