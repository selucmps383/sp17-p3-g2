export interface MatchHistory
{
    matchHistory: {
        endIndex: number;
        matches: any[];
        startIndex: number;
        totalGames: number
    }
}