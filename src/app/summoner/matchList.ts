export interface MatchList
{
    timestamp: number;
    champion: number;
    region: string;
    queue: string;
    season: string;
    matchId: number;
    role: string;
    platformId: string;
    lane: string;
}