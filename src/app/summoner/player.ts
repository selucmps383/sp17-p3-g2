export interface Player
{
    player:
    {
        id: number;
        name: string;
        profileIconId?: number;
        revisionDate: number;
        summonerLevel: number;
    }
}