import { Component, Injectable, Inject, ElementRef} from '@angular/core';
import { Http, Response } from '@angular/http';
import { SummonerService } from './summoner.service'
import { Player } from './player';
import { RecentGames } from './recentGames';
import { MatchHistory } from './matchHistory';
import { MatchList } from './matchList';
import { RecentGamesDto } from './RecentGames/RecentGamesDto';
import { ChampionDto } from './Champion/ChampionDto';
import { Item } from './Item/item';
import { SummonerDto } from './SummonerDto';
import { Observable } from 'rxjs/Rx';
import { DataHandlerService } from './data-handler.service';
import { DataHandler } from './data-handler';
import 'rxjs/add/operator/map';


//LASCIATE OGNE SPERANZA, VOI CH'INTRATE
//DISCLAIMER: Arrays were being stupid in typescript, therefore, RIP sanity. 
@Component({
  selector: 'app-summoner',
  templateUrl: './summoner.component.html',
  styleUrls: ['./summoner.component.css']
})

//I am aware that this code is ugly, inefficient, and there are probably simpler ways to do it, but every simple method I tried failed :(
export class SummonerComponent{

  endIndex: number;
  startIndex: number;
  totalGames: number;
  allMatches: MatchList[];
  summonerId: number = 0;
  summonerName: string = '';
  recentGames: RecentGamesDto;

  champ: ChampionDto;
  item: Item;
  

  champId_0: number = 0;
  item0_0: number = 0;
  item1_0: number = 0;
  item2_0: number = 0;
  item3_0: number = 0;
  item4_0: number = 0;
  item5_0: number = 0;
  item6_0: number = 0;
  status_0: boolean = false;
  champName_0: string = '';
  cName0: string = '';
  item0_0text: string = '';
  item1_0text: string = '';
  item2_0text: string = '';
  item3_0text: string = '';
  item4_0text: string = '';
  item5_0text: string = '';
  item6_0text: string = '';

  champId_1: number = 0;
  item0_1: number = 0;
  item1_1: number = 0;
  item2_1: number = 0;
  item3_1: number = 0;
  item4_1: number = 0;
  item5_1: number = 0;
  item6_1: number = 0;
  status_1: boolean = false;
  champName_1: string = '';
  cName1: string = '';
  item0_1text: string = '';
  item1_1text: string = '';
  item2_1text: string = '';
  item3_1text: string = '';
  item4_1text: string = '';
  item5_1text: string = '';
  item6_1text: string = '';


  champId_2: number = 0;
  item0_2: number = 0;
  item1_2: number = 0;
  item2_2: number = 0;
  item3_2: number = 0;
  item4_2: number = 0;
  item5_2: number = 0;
  item6_2: number = 0;
  status_2: boolean = false;
  champName_2: string = '';
  cName2: string = '';
  item0_2text: string = '';
  item1_2text: string = '';
  item2_2text: string = '';
  item3_2text: string = '';
  item4_2text: string = '';
  item5_2text: string = '';
  item6_2text: string = '';

  champId_3: number = 0;
  item0_3: number = 0;
  item1_3: number = 0;
  item2_3: number = 0;
  item3_3: number = 0;
  item4_3: number = 0;
  item5_3: number = 0;
  item6_3: number = 0;
  status_3: boolean = false;
  champName_3: string = '';
  cName3: string = '';
  item0_3text: string = '';
  item1_3text: string = '';
  item2_3text: string = '';
  item3_3text: string = '';
  item4_3text: string = '';
  item5_3text: string = '';
  item6_3text: string = '';

  champId_4: number = 0;
  item0_4: number = 0;
  item1_4: number = 0;
  item2_4: number = 0;
  item3_4: number = 0;
  item4_4: number = 0;
  item5_4: number = 0;
  item6_4: number = 0;
  status_4: boolean = false;
  champName_4: string = '';
  cName4: string = '';
  item0_4text: string = '';
  item1_4text: string = '';
  item2_4text: string = '';
  item3_4text: string = '';
  item4_4text: string = '';
  item5_4text: string = '';
  item6_4text: string = '';

  champId_5: number = 0;
  item0_5: number = 0;
  item1_5: number = 0;
  item2_5: number = 0;
  item3_5: number = 0;
  item4_5: number = 0;
  item5_5: number = 0;
  item6_5: number = 0;
  status_5: boolean = false;
  champName_5: string = '';
  champ_5: ChampionDto;
  cName5: string = '';
  item0_5text: string = '';
  item1_5text: string = '';
  item2_5text: string = '';
  item3_5text: string = '';
  item4_5text: string = '';
  item5_5text: string = '';
  item6_5text: string = '';

  champId_6: number = 0;
  item0_6: number = 0;
  item1_6: number = 0;
  item2_6: number = 0;
  item3_6: number = 0;
  item4_6: number = 0;
  item5_6: number = 0;
  item6_6: number = 0;
  status_6: boolean = false;
  champName_6: string = '';
  cName6: string = '';
  item0_6text: string = '';
  item1_6text: string = '';
  item2_6text: string = '';
  item3_6text: string = '';
  item4_6text: string = '';
  item5_6text: string = '';
  item6_6text: string = '';

  champId_7: number = 0;
  item0_7: number = 0;
  item1_7: number = 0;
  item2_7: number = 0;
  item3_7: number = 0;
  item4_7: number = 0;
  item5_7: number = 0;
  item6_7: number = 0;
  status_7: boolean = false;
  champName_7: string = '';
  cName7: string = '';
  item0_7text: string = '';
  item1_7text: string = '';
  item2_7text: string = '';
  item3_7text: string = '';
  item4_7text: string = '';
  item5_7text: string = '';
  item6_7text: string = '';

  champId_8: number = 0;
  item0_8: number = 0;
  item1_8: number = 0;
  item2_8: number = 0;
  item3_8: number = 0;
  item4_8: number = 0;
  item5_8: number = 0;
  item6_8: number = 0;
  status_8: boolean = false;
  champName_8: string = '';
  cName8: string = '';
  item0_8text: string = '';
  item1_8text: string = '';
  item2_8text: string = '';
  item3_8text: string = '';
  item4_8text: string = '';
  item5_8text: string = '';
  item6_8text: string = '';

  champId_9: number = 0;
  item0_9: number = 0;
  item1_9: number = 0;
  item2_9: number = 0;
  item3_9: number = 0;
  item4_9: number = 0;
  item5_9: number = 0;
  item6_9: number = 0;
  status_9: boolean = false;
  champName_9: string = '';
  cName9: string = '';
  item0_9text: string = '';
  item1_9text: string = '';
  item2_9text: string = '';
  item3_9text: string = '';
  item4_9text: string = '';
  item5_9text: string = '';
  item6_9text: string = '';


  Searched: boolean = false;
  MoreInfo0: boolean = false;
  MoreInfo1: boolean = false;
  MoreInfo2: boolean = false;
  MoreInfo3: boolean = false;
  MoreInfo4: boolean = false;
  MoreInfo5: boolean = false;
  MoreInfo6: boolean = false;
  MoreInfo7: boolean = false;
  MoreInfo8: boolean = false;
  MoreInfo9: boolean = false;

  match0_player0Id: number = 0;
  match0_player0Name: string = '';

  match0_player1Id: number = 0;
  match0_player1Name: string = '';

  match0_player2Id: number = 0;
  match0_player2Name: string = '';

  match0_player3Id: number = 0;
  match0_player3Name: string = '';

  match0_player4Id: number = 0;
  match0_player4Name: string = '';

  match0_player5Id: number = 0;
  match0_player5Name: string = '';

  match0_player6Id: number = 0;
  match0_player6Name: string = '';

  match0_player7Id: number = 0;
  match0_player7Name: string = '';

  match0_player8Id: number = 0;
  match0_player8Name: string = '';

  match1_player0Id: number = 0;
  match1_player0Name: string = '';

  match1_player1Id: number = 0;
  match1_player1Name: string = '';

  match1_player2Id: number = 0;
  match1_player2Name: string = '';

  match1_player3Id: number = 0;
  match1_player3Name: string = '';

  match1_player4Id: number = 0;
  match1_player4Name: string = '';

  match1_player5Id: number = 0;
  match1_player5Name: string = '';

  match1_player6Id: number = 0;
  match1_player6Name: string = '';

  match1_player7Id: number = 0;
  match1_player7Name: string = '';

  match1_player8Id: number = 0;
  match1_player8Name: string = '';

  match2_player0Id: number = 0;
  match2_player0Name: string = '';

  match2_player1Id: number = 0;
  match2_player1Name: string = '';

  match2_player2Id: number = 0;
  match2_player2Name: string = '';

  match2_player3Id: number = 0;
  match2_player3Name: string = '';

  match2_player4Id: number = 0;
  match2_player4Name: string = '';

  match2_player5Id: number = 0;
  match2_player5Name: string = '';

  match2_player6Id: number = 0;
  match2_player6Name: string = '';

  match2_player7Id: number = 0;
  match2_player7Name: string = '';

  match2_player8Id: number = 0;
  match2_player8Name: string = '';

  match3_player0Id: number = 0;
  match3_player0Name: string = '';

  match3_player1Id: number = 0;
  match3_player1Name: string = '';

  match3_player2Id: number = 0;
  match3_player2Name: string = '';

  match3_player3Id: number = 0;
  match3_player3Name: string = '';

  match3_player4Id: number = 0;
  match3_player4Name: string = '';

  match3_player5Id: number = 0;
  match3_player5Name: string = '';

  match3_player6Id: number = 0;
  match3_player6Name: string = '';

  match3_player7Id: number = 0;
  match3_player7Name: string = '';

  match3_player8Id: number = 0;
  match3_player8Name: string = '';

  match4_player0Id: number = 0;
  match4_player0Name: string = '';

  match4_player1Id: number = 0;
  match4_player1Name: string = '';

  match4_player2Id: number = 0;
  match4_player2Name: string = '';

  match4_player3Id: number = 0;
  match4_player3Name: string = '';

  match4_player4Id: number = 0;
  match4_player4Name: string = '';

  match4_player5Id: number = 0;
  match4_player5Name: string = '';

  match4_player6Id: number = 0;
  match4_player6Name: string = '';

  match4_player7Id: number = 0;
  match4_player7Name: string = '';

  match4_player8Id: number = 0;
  match4_player8Name: string = '';

  match5_player0Id: number = 0;
  match5_player0Name: string = '';

  match5_player1Id: number = 0;
  match5_player1Name: string = '';

  match5_player2Id: number = 0;
  match5_player2Name: string = '';

  match5_player3Id: number = 0;
  match5_player3Name: string = '';

  match5_player4Id: number = 0;
  match5_player4Name: string = '';

  match5_player5Id: number = 0;
  match5_player5Name: string = '';

  match5_player6Id: number = 0;
  match5_player6Name: string = '';

  match5_player7Id: number = 0;
  match5_player7Name: string = '';

  match5_player8Id: number = 0;
  match5_player8Name: string = '';

  match6_player0Id: number = 0;
  match6_player0Name: string = '';

  match6_player1Id: number = 0;
  match6_player1Name: string = '';

  match6_player2Id: number = 0;
  match6_player2Name: string = '';

  match6_player3Id: number = 0;
  match6_player3Name: string = '';

  match6_player4Id: number = 0;
  match6_player4Name: string = '';

  match6_player5Id: number = 0;
  match6_player5Name: string = '';

  match6_player6Id: number = 0;
  match6_player6Name: string = '';

  match6_player7Id: number = 0;
  match6_player7Name: string = '';

  match6_player8Id: number = 0;
  match6_player8Name: string = '';

  match7_player0Id: number = 0;
  match7_player0Name: string = '';

  match7_player1Id: number = 0;
  match7_player1Name: string = '';

  match7_player2Id: number = 0;
  match7_player2Name: string = '';

  match7_player3Id: number = 0;
  match7_player3Name: string = '';

  match7_player4Id: number = 0;
  match7_player4Name: string = '';

  match7_player5Id: number = 0;
  match7_player5Name: string = '';

  match7_player6Id: number = 0;
  match7_player6Name: string = '';

  match7_player7Id: number = 0;
  match7_player7Name: string = '';

  match7_player8Id: number = 0;
  match7_player8Name: string = '';

  match8_player0Id: number = 0;
  match8_player0Name: string = '';

  match8_player1Id: number = 0;
  match8_player1Name: string = '';

  match8_player2Id: number = 0;
  match8_player2Name: string = '';

  match8_player3Id: number = 0;
  match8_player3Name: string = '';

  match8_player4Id: number = 0;
  match8_player4Name: string = '';

  match8_player5Id: number = 0;
  match8_player5Name: string = '';

  match8_player6Id: number = 0;
  match8_player6Name: string = '';

  match8_player7Id: number = 0;
  match8_player7Name: string = '';

  match8_player8Id: number = 0;
  match8_player8Name: string = '';
  
  match9_player0Id: number = 0;
  match9_player0Name: string = '';

  match9_player1Id: number = 0;
  match9_player1Name: string = '';

  match9_player2Id: number = 0;
  match9_player2Name: string = '';

  match9_player3Id: number = 0;
  match9_player3Name: string = '';

  match9_player4Id: number = 0;
  match9_player4Name: string = '';

  match9_player5Id: number = 0;
  match9_player5Name: string = '';

  match9_player6Id: number = 0;
  match9_player6Name: string = '';

  match9_player7Id: number = 0;
  match9_player7Name: string = '';

  match9_player8Id: number = 0;
  match9_player8Name: string = '';

  summonerNameData: SummonerDto;
  


  constructor(private summonerService: SummonerService, private dataService: DataHandlerService) { }


  //note that we are just passing "scarra" in right now rather than
  //taking what the person typed. this is for testing purposes
  findSummoner(name: string)
  {
      //reinit everything if someone doesn't have 10 games (APPARENTLY SINGLE LINE DECLARATIONS DON'T WORK IN ANGULAR2 -_-)
        this.status_0 = false;
        this.status_1 = false;
        this.status_2 = false;
        this.status_3 = false
        this.status_4 = false;
        this.status_5 = false;
        this.status_6 = false;
        this.status_7 = false;
        this.status_8 = false;
        this.status_9 = false;

        this.champId_0 = 0;
        this.item0_0 = 0;
        this.item1_0 = 0;
        this.item2_0 = 0;
        this.item3_0 = 0;
        this.item4_0 = 0;
        this.item5_0 = 0;
        this.item6_0 = 0;
        this.champName_0 = '';
        this.cName0 = '';
        this.item0_0text = '';
        this.item1_0text = '';
        this.item2_0text = '';
        this.item3_0text = '';
        this.item4_0text = '';
        this.item5_0text = '';
        this.item6_0text = '';

        this.champId_1 = 0;
        this.item0_1 = 0;
        this.item1_1 = 0;
        this.item2_1 = 0;
        this.item3_1 = 0;
        this.item4_1 = 0;
        this.item5_1 = 0;
        this.item6_1 = 0;
        this.champName_1 = '';
        this.cName1 = '';
        this.item0_1text = '';
        this.item1_1text = '';
        this.item2_1text = '';
        this.item3_1text = '';
        this.item4_1text = '';
        this.item5_1text = '';
        this.item6_1text = '';

        this.champId_2 = 0;
        this.item0_2 = 0;
        this.item1_2 = 0;
        this.item2_2 = 0;
        this.item3_2 = 0;
        this.item4_2 = 0;
        this.item5_2 = 0;
        this.item6_2 = 0;
        this.champName_2 = '';
        this.cName2 = '';
        this.item0_2text = '';
        this.item1_2text = '';
        this.item2_2text = '';
        this.item3_2text = '';
        this.item4_2text = '';
        this.item5_2text = '';
        this.item6_2text = '';

        this.champId_3 = 0;
        this.item0_3 = 0;
        this.item1_3 = 0;
        this.item2_3 = 0;
        this.item3_3 = 0;
        this.item4_3 = 0;
        this.item5_3 = 0;
        this.item6_3 = 0;
        this.champName_3 = '';
        this.cName3 = '';
        this.item0_3text = '';
        this.item1_3text = '';
        this.item2_3text = '';
        this.item3_3text = '';
        this.item4_3text = '';
        this.item5_3text = '';
        this.item6_3text = '';

        this.champId_4 = 0;
        this.item0_4 = 0;
        this.item1_4 = 0;
        this.item2_4 = 0;
        this.item3_4 = 0;
        this.item4_4 = 0;
        this.item5_4 = 0;
        this.item6_4 = 0;
        this.champName_4 = '';
        this.cName4 = '';
        this.item0_4text = '';
        this.item1_4text = '';
        this.item2_4text = '';
        this.item3_4text = '';
        this.item4_4text = '';
        this.item5_4text = '';
        this.item6_4text = '';

        this.champId_5 = 0;
        this.item0_5 = 0;
        this.item1_5 = 0;
        this.item2_5 = 0;
        this.item3_5 = 0;
        this.item4_5 = 0;
        this.item5_5 = 0;
        this.item6_5 = 0;
        this.champName_5 = '';
        this.cName5 = '';
        this.item0_5text = '';
        this.item1_5text = '';
        this.item2_5text = '';
        this.item3_5text = '';
        this.item4_5text = '';
        this.item5_5text = '';
        this.item6_5text = '';

        this.champId_6 = 0;
        this.item0_6 = 0;
        this.item1_6 = 0;
        this.item2_6 = 0;
        this.item3_6 = 0;
        this.item4_6 = 0;
        this.item5_6 = 0;
        this.item6_6 = 0;
        this.champName_6 = '';
        this.cName6 = '';
        this.item0_6text = '';
        this.item1_6text = '';
        this.item2_6text = '';
        this.item3_6text = '';
        this.item4_6text = '';
        this.item5_6text = '';
        this.item6_6text = '';

        this.champId_7 = 0;
        this.item0_7 = 0;
        this.item1_7 = 0;
        this.item2_7 = 0;
        this.item3_7 = 0;
        this.item4_7 = 0;
        this.item5_7 = 0;
        this.item6_7 = 0;
        this.champName_7 = '';
        this.cName7 = '';
        this.item0_7text = '';
        this.item1_7text = '';
        this.item2_7text = '';
        this.item3_7text = '';
        this.item4_7text = '';
        this.item5_7text = '';
        this.item6_7text = '';

        this.champId_8 = 0;
        this.item0_8 = 0;
        this.item1_8 = 0;
        this.item2_8 = 0;
        this.item3_8 = 0;
        this.item4_8 = 0;
        this.item5_8 = 0;
        this.item6_8 = 0;
        this.champName_8 = '';
        this.cName8 = '';
        this.item0_8text = '';
        this.item1_8text = '';
        this.item2_8text = '';
        this.item3_8text = '';
        this.item4_8text = '';
        this.item5_8text = '';
        this.item6_8text = '';

        this.champId_9 = 0;
        this.item0_9 = 0;
        this.item1_9 = 0;
        this.item2_9 = 0;
        this.item3_9 = 0;
        this.item4_9 = 0;
        this.item5_9 = 0;
        this.item6_9 = 0;
        this.champName_9 = '';
        this.cName9 = '';
        this.item0_9text = '';
        this.item1_9text = '';
        this.item2_9text = '';
        this.item3_9text = '';
        this.item4_9text = '';
        this.item5_9text = '';
        this.item6_9text = '';

        
        this.match0_player0Name = '';
        this.match0_player1Name = '';
        this.match0_player2Name = '';
        this.match0_player3Name = '';
        this.match0_player4Name= '';
        this.match0_player5Name= '';
        this.match0_player6Name= '';
        this.match0_player7Name= '';
        this.match0_player8Name= '';
        this.match1_player0Name= '';
        this.match1_player1Name= '';
        this.match1_player2Name= '';
        this.match1_player3Name= '';
        this.match1_player4Name= '';
        this.match1_player5Name= '';
        this.match1_player6Name= '';
        this.match1_player7Name= '';
        this.match1_player8Name= '';
        this.match2_player0Name= '';
        this.match2_player1Name= '';
        this.match2_player2Name= '';
        this.match2_player3Name= '';
        this.match2_player4Name= '';
        this.match2_player5Name= '';
        this.match2_player6Name= '';
        this.match2_player7Name= '';
        this.match2_player8Name= '';
        this.match3_player0Name= '';
        this.match3_player1Name= '';
        this.match3_player2Name= '';
        this.match3_player3Name= '';
        this.match3_player4Name= '';
        this.match3_player5Name= '';
        this.match3_player6Name= '';
        this.match3_player7Name= '';
        this.match3_player8Name= '';
        this.match4_player0Name= '';
        this.match4_player1Name= '';
        this.match4_player2Name= '';
        this.match4_player3Name= '';
        this.match4_player4Name= '';
        this.match4_player5Name= '';
        this.match4_player6Name= '';
        this.match4_player7Name= '';
        this.match4_player8Name= '';
        this.match5_player0Name= '';
        this.match5_player1Name= '';
        this.match5_player2Name= '';
        this.match5_player3Name= '';
        this.match5_player4Name= '';
        this.match5_player5Name= '';
        this.match5_player6Name= '';
        this.match5_player7Name= '';
        this.match5_player8Name= '';
        this.match6_player0Name= '';
        this.match6_player1Name= '';
        this.match6_player2Name= '';
        this.match6_player3Name= '';
        this.match6_player4Name= '';
        this.match6_player5Name= '';
        this.match6_player6Name= '';
        this.match6_player7Name= '';
        this.match6_player8Name= '';
        this.match7_player0Name= '';
        this.match7_player1Name= '';
        this.match7_player2Name= '';
        this.match7_player3Name= '';
        this.match7_player4Name= '';
        this.match7_player5Name= '';
        this.match7_player6Name= '';
        this.match7_player7Name= '';
        this.match7_player8Name= '';
        this.match8_player0Name= '';
        this.match8_player1Name= '';
        this.match8_player2Name= '';
        this.match8_player3Name= '';
        this.match8_player4Name= '';
        this.match8_player5Name= '';
        this.match8_player6Name= '';
        this.match8_player7Name= '';
        this.match8_player8Name= '';
        this.match9_player0Name= '';
        this.match9_player1Name= '';
        this.match9_player2Name= '';
        this.match9_player3Name= '';
        this.match9_player4Name= '';
        this.match9_player5Name= '';
        this.match9_player6Name= '';
        this.match9_player7Name= '';
        this.match9_player8Name= '';

        this.match0_player0Id = 0;
        this.match0_player1Id = 0;
        this.match0_player2Id = 0;
        this.match0_player3Id = 0;
        this.match0_player4Id = 0;
        this.match0_player5Id = 0;
        this.match0_player6Id = 0;
        this.match0_player7Id = 0;
        this.match0_player8Id = 0;
        this.match1_player0Id = 0;
        this.match1_player1Id = 0;
        this.match1_player2Id = 0;
        this.match1_player3Id = 0;
        this.match1_player4Id = 0;
        this.match1_player5Id = 0;
        this.match1_player6Id = 0;
        this.match1_player7Id = 0;
        this.match1_player8Id = 0;
        this.match2_player0Id = 0;
        this.match2_player1Id = 0;
        this.match2_player2Id = 0;
        this.match2_player3Id = 0;
        this.match2_player4Id = 0;
        this.match2_player5Id = 0;
        this.match2_player6Id = 0;
        this.match2_player7Id = 0;
        this.match2_player8Id = 0;
        this.match3_player0Id = 0;
        this.match3_player1Id = 0;
        this.match3_player2Id = 0;
        this.match3_player3Id = 0;
        this.match3_player4Id = 0;
        this.match3_player5Id = 0;
        this.match3_player6Id = 0;
        this.match3_player7Id = 0;
        this.match3_player8Id = 0;
        this.match4_player0Id = 0;
        this.match4_player1Id = 0;
        this.match4_player2Id = 0;
        this.match4_player3Id = 0;
        this.match4_player4Id = 0;
        this.match4_player5Id = 0;
        this.match4_player6Id = 0;
        this.match4_player7Id = 0;
        this.match4_player8Id = 0;
        this.match5_player0Id = 0;
        this.match5_player1Id = 0;
        this.match5_player2Id = 0;
        this.match5_player3Id = 0;
        this.match5_player4Id = 0;
        this.match5_player5Id = 0;
        this.match5_player6Id = 0;
        this.match5_player7Id = 0;
        this.match5_player8Id = 0;
        this.match6_player0Id = 0;
        this.match6_player1Id = 0;
        this.match6_player2Id = 0;
        this.match6_player3Id = 0;
        this.match6_player4Id = 0;
        this.match6_player5Id = 0;
        this.match6_player6Id = 0;
        this.match6_player7Id = 0;
        this.match6_player8Id = 0;
        this.match7_player0Id = 0;
        this.match7_player1Id = 0;
        this.match7_player2Id = 0;
        this.match7_player3Id = 0;
        this.match7_player4Id = 0;
        this.match7_player5Id = 0;
        this.match7_player6Id = 0;
        this.match7_player7Id = 0;
        this.match7_player8Id = 0;
        this.match8_player0Id = 0;
        this.match8_player1Id = 0;
        this.match8_player2Id = 0;
        this.match8_player3Id = 0;
        this.match8_player4Id = 0;
        this.match8_player5Id = 0;
        this.match8_player6Id = 0;
        this.match8_player7Id = 0;
        this.match8_player8Id = 0;
        this.match9_player0Id = 0;
        this.match9_player1Id = 0;
        this.match9_player2Id = 0;
        this.match9_player3Id = 0;
        this.match9_player4Id = 0;
        this.match9_player5Id = 0;
        this.match9_player6Id = 0;
        this.match9_player7Id = 0;
        this.match9_player8Id = 0;

        this.MoreInfo0 = false;
        this.MoreInfo1 = false;
        this.MoreInfo2 = false;
        this.MoreInfo3 = false;
        this.MoreInfo4 = false;
        this.MoreInfo5 = false;
        this.MoreInfo6 = false;
        this.MoreInfo7 = false;
        this.MoreInfo8 = false;
        this.MoreInfo9 = false;

  

      this.summonerName = name;
      this.summonerService.getPlayer(name)
        .subscribe((response: any) => 
        {
                this.dataService.data.player = response;
                //this.summoner = response;
                //this.data = JSON.stringify(this.summoner);
                let t;
                for(var prop in this.dataService.data){
                  //console.log(prop);
                  //console.log(this.dataService.data[prop]);
                  for (var iprop in this.dataService.data[prop]){
                    //console.log(iprop);
                    //console.log(this.dataService.data[prop][iprop]);
                    t = this.dataService.data[prop][iprop];
                    this.summonerId = t.id;

                    //alert(`${t.id}`);
                    this.findGames(this.summonerId);
                  }
                }
        });
  }

  findGames(summonerId: number)
  {
    this.summonerService.getGamesList(summonerId)
      .subscribe(response => {
          this.recentGames = response;

          this.champId_0 = this.recentGames.games[0].championId;
          this.item0_0 = this.recentGames.games[0].stats.item0;
          this.item1_0 = this.recentGames.games[0].stats.item1;
          this.item2_0 = this.recentGames.games[0].stats.item2;
          this.item3_0 = this.recentGames.games[0].stats.item3;
          this.item4_0 = this.recentGames.games[0].stats.item4;
          this.item5_0 = this.recentGames.games[0].stats.item5;
          this.item6_0 = this.recentGames.games[0].stats.item6;
          this.status_0 = this.recentGames.games[0].stats.win;

          

          this.champId_1 = this.recentGames.games[1].championId;
          this.item0_1 = this.recentGames.games[1].stats.item0;
          this.item1_1 = this.recentGames.games[1].stats.item1;
          this.item2_1 = this.recentGames.games[1].stats.item2;
          this.item3_1 = this.recentGames.games[1].stats.item3;
          this.item4_1 = this.recentGames.games[1].stats.item4;
          this.item5_1 = this.recentGames.games[1].stats.item5;
          this.item6_1 = this.recentGames.games[1].stats.item6;
          this.status_1 = this.recentGames.games[1].stats.win;

          this.champId_2 = this.recentGames.games[2].championId;
          this.item0_2 = this.recentGames.games[2].stats.item0;
          this.item1_2 = this.recentGames.games[2].stats.item1;
          this.item2_2 = this.recentGames.games[2].stats.item2;
          this.item3_2 = this.recentGames.games[2].stats.item3;
          this.item4_2 = this.recentGames.games[2].stats.item4;
          this.item5_2 = this.recentGames.games[2].stats.item5;
          this.item6_2 = this.recentGames.games[2].stats.item6;
          this.status_2 = this.recentGames.games[2].stats.win;

          this.champId_3 = this.recentGames.games[3].championId;
          this.item0_3 = this.recentGames.games[3].stats.item0;
          this.item1_3 = this.recentGames.games[3].stats.item1;
          this.item2_3 = this.recentGames.games[3].stats.item2;
          this.item3_3 = this.recentGames.games[3].stats.item3;
          this.item4_3 = this.recentGames.games[3].stats.item4;
          this.item5_3 = this.recentGames.games[3].stats.item5;
          this.item6_3 = this.recentGames.games[3].stats.item6;
          this.status_3 = this.recentGames.games[3].stats.win;

          this.champId_4 = this.recentGames.games[4].championId;
          this.item0_4 = this.recentGames.games[4].stats.item0;
          this.item1_4 = this.recentGames.games[4].stats.item1;
          this.item2_4 = this.recentGames.games[4].stats.item2;
          this.item3_4 = this.recentGames.games[4].stats.item3;
          this.item4_4 = this.recentGames.games[4].stats.item4;
          this.item5_4 = this.recentGames.games[4].stats.item5;
          this.item6_4 = this.recentGames.games[4].stats.item6;
          this.status_4 = this.recentGames.games[4].stats.win;

          this.champId_5 = this.recentGames.games[5].championId;
          this.item0_5 = this.recentGames.games[5].stats.item0;
          this.item1_5 = this.recentGames.games[5].stats.item1;
          this.item2_5 = this.recentGames.games[5].stats.item2;
          this.item3_5 = this.recentGames.games[5].stats.item3;
          this.item4_5 = this.recentGames.games[5].stats.item4;
          this.item5_5 = this.recentGames.games[5].stats.item5;
          this.item6_5 = this.recentGames.games[5].stats.item6;
          this.status_5 = this.recentGames.games[5].stats.win;

          this.champId_6 = this.recentGames.games[6].championId;
          this.item0_6 = this.recentGames.games[6].stats.item0;
          this.item1_6 = this.recentGames.games[6].stats.item1;
          this.item2_6 = this.recentGames.games[6].stats.item2;
          this.item3_6 = this.recentGames.games[6].stats.item3;
          this.item4_6 = this.recentGames.games[6].stats.item4;
          this.item5_6 = this.recentGames.games[6].stats.item5;
          this.item6_6 = this.recentGames.games[6].stats.item6;
          this.status_6 = this.recentGames.games[6].stats.win;

          this.champId_7 = this.recentGames.games[7].championId;
          this.item0_7 = this.recentGames.games[7].stats.item0;
          this.item1_7 = this.recentGames.games[7].stats.item1;
          this.item2_7 = this.recentGames.games[7].stats.item2;
          this.item3_7 = this.recentGames.games[7].stats.item3;
          this.item4_7 = this.recentGames.games[7].stats.item4;
          this.item5_7 = this.recentGames.games[7].stats.item5;
          this.item6_7 = this.recentGames.games[7].stats.item6;
          this.status_7 = this.recentGames.games[7].stats.win;

          this.champId_8 = this.recentGames.games[8].championId;
          this.item0_8 = this.recentGames.games[8].stats.item0;
          this.item1_8 = this.recentGames.games[8].stats.item1;
          this.item2_8 = this.recentGames.games[8].stats.item2;
          this.item3_8 = this.recentGames.games[8].stats.item3;
          this.item4_8 = this.recentGames.games[8].stats.item4;
          this.item5_8 = this.recentGames.games[8].stats.item5;
          this.item6_8 = this.recentGames.games[8].stats.item6;
          this.status_8 = this.recentGames.games[8].stats.win;

          this.champId_9 = this.recentGames.games[9].championId;
          this.item0_9 = this.recentGames.games[9].stats.item0;
          this.item1_9 = this.recentGames.games[9].stats.item1;
          this.item2_9 = this.recentGames.games[9].stats.item2;
          this.item3_9 = this.recentGames.games[9].stats.item3;
          this.item4_9 = this.recentGames.games[9].stats.item4;
          this.item5_9 = this.recentGames.games[9].stats.item5;
          this.item6_9 = this.recentGames.games[9].stats.item6;
          this.status_9 = this.recentGames.games[9].stats.win;

          this.match0_player0Id = this.recentGames.games[0].fellowPlayers[0].summonerId;
          this.match0_player1Id = this.recentGames.games[0].fellowPlayers[1].summonerId;
          this.match0_player2Id = this.recentGames.games[0].fellowPlayers[2].summonerId;
          this.match0_player3Id = this.recentGames.games[0].fellowPlayers[3].summonerId;
          this.match0_player4Id = this.recentGames.games[0].fellowPlayers[4].summonerId;
          this.match0_player5Id = this.recentGames.games[0].fellowPlayers[5].summonerId;
          this.match0_player6Id = this.recentGames.games[0].fellowPlayers[6].summonerId;
          this.match0_player7Id = this.recentGames.games[0].fellowPlayers[7].summonerId;
          this.match0_player8Id = this.recentGames.games[0].fellowPlayers[8].summonerId;

          this.match1_player0Id = this.recentGames.games[1].fellowPlayers[0].summonerId;
          this.match1_player1Id = this.recentGames.games[1].fellowPlayers[1].summonerId;
          this.match1_player2Id = this.recentGames.games[1].fellowPlayers[2].summonerId;
          this.match1_player3Id = this.recentGames.games[1].fellowPlayers[3].summonerId;
          this.match1_player4Id = this.recentGames.games[1].fellowPlayers[4].summonerId;
          this.match1_player5Id = this.recentGames.games[1].fellowPlayers[5].summonerId;
          this.match1_player6Id = this.recentGames.games[1].fellowPlayers[6].summonerId;
          this.match1_player7Id = this.recentGames.games[1].fellowPlayers[7].summonerId;
          this.match1_player8Id = this.recentGames.games[1].fellowPlayers[8].summonerId;

          this.match2_player0Id = this.recentGames.games[2].fellowPlayers[0].summonerId;
          this.match2_player1Id = this.recentGames.games[2].fellowPlayers[1].summonerId;
          this.match2_player2Id = this.recentGames.games[2].fellowPlayers[2].summonerId;
          this.match2_player3Id = this.recentGames.games[2].fellowPlayers[3].summonerId;
          this.match2_player4Id = this.recentGames.games[2].fellowPlayers[4].summonerId;
          this.match2_player5Id = this.recentGames.games[2].fellowPlayers[5].summonerId;
          this.match2_player6Id = this.recentGames.games[2].fellowPlayers[6].summonerId;
          this.match2_player7Id = this.recentGames.games[2].fellowPlayers[7].summonerId;
          this.match2_player8Id = this.recentGames.games[2].fellowPlayers[8].summonerId;

          this.match3_player0Id = this.recentGames.games[3].fellowPlayers[0].summonerId;
          this.match3_player1Id = this.recentGames.games[3].fellowPlayers[1].summonerId;
          this.match3_player2Id = this.recentGames.games[3].fellowPlayers[2].summonerId;
          this.match3_player3Id = this.recentGames.games[3].fellowPlayers[3].summonerId;
          this.match3_player4Id = this.recentGames.games[3].fellowPlayers[4].summonerId;
          this.match3_player5Id = this.recentGames.games[3].fellowPlayers[5].summonerId;
          this.match3_player6Id = this.recentGames.games[3].fellowPlayers[6].summonerId;
          this.match3_player7Id = this.recentGames.games[3].fellowPlayers[7].summonerId;
          this.match3_player8Id = this.recentGames.games[3].fellowPlayers[8].summonerId;

          this.match4_player0Id = this.recentGames.games[4].fellowPlayers[0].summonerId;
          this.match4_player1Id = this.recentGames.games[4].fellowPlayers[1].summonerId;
          this.match4_player2Id = this.recentGames.games[4].fellowPlayers[2].summonerId;
          this.match4_player3Id = this.recentGames.games[4].fellowPlayers[3].summonerId;
          this.match4_player4Id = this.recentGames.games[4].fellowPlayers[4].summonerId;
          this.match4_player5Id = this.recentGames.games[4].fellowPlayers[5].summonerId;
          this.match4_player6Id = this.recentGames.games[4].fellowPlayers[6].summonerId;
          this.match4_player7Id = this.recentGames.games[4].fellowPlayers[7].summonerId;
          this.match4_player8Id = this.recentGames.games[4].fellowPlayers[8].summonerId;

          this.match5_player0Id = this.recentGames.games[5].fellowPlayers[0].summonerId;
          this.match5_player1Id = this.recentGames.games[5].fellowPlayers[1].summonerId;
          this.match5_player2Id = this.recentGames.games[5].fellowPlayers[2].summonerId;
          this.match5_player3Id = this.recentGames.games[5].fellowPlayers[3].summonerId;
          this.match5_player4Id = this.recentGames.games[5].fellowPlayers[4].summonerId;
          this.match5_player5Id = this.recentGames.games[5].fellowPlayers[5].summonerId;
          this.match5_player6Id = this.recentGames.games[5].fellowPlayers[6].summonerId;
          this.match5_player7Id = this.recentGames.games[5].fellowPlayers[7].summonerId;
          this.match5_player8Id = this.recentGames.games[5].fellowPlayers[8].summonerId;

          this.match6_player0Id = this.recentGames.games[6].fellowPlayers[0].summonerId;
          this.match6_player1Id = this.recentGames.games[6].fellowPlayers[1].summonerId;
          this.match6_player2Id = this.recentGames.games[6].fellowPlayers[2].summonerId;
          this.match6_player3Id = this.recentGames.games[6].fellowPlayers[3].summonerId;
          this.match6_player4Id = this.recentGames.games[6].fellowPlayers[4].summonerId;
          this.match6_player5Id = this.recentGames.games[6].fellowPlayers[5].summonerId;
          this.match6_player6Id = this.recentGames.games[6].fellowPlayers[6].summonerId;
          this.match6_player7Id = this.recentGames.games[6].fellowPlayers[7].summonerId;
          this.match6_player8Id = this.recentGames.games[6].fellowPlayers[8].summonerId;

          this.match7_player0Id = this.recentGames.games[7].fellowPlayers[0].summonerId;
          this.match7_player1Id = this.recentGames.games[7].fellowPlayers[1].summonerId;
          this.match7_player2Id = this.recentGames.games[7].fellowPlayers[2].summonerId;
          this.match7_player3Id = this.recentGames.games[7].fellowPlayers[3].summonerId;
          this.match7_player4Id = this.recentGames.games[7].fellowPlayers[4].summonerId;
          this.match7_player5Id = this.recentGames.games[7].fellowPlayers[5].summonerId;
          this.match7_player6Id = this.recentGames.games[7].fellowPlayers[6].summonerId;
          this.match7_player7Id = this.recentGames.games[7].fellowPlayers[7].summonerId;
          this.match7_player8Id = this.recentGames.games[7].fellowPlayers[8].summonerId;

          this.match8_player0Id = this.recentGames.games[8].fellowPlayers[0].summonerId;
          this.match8_player1Id = this.recentGames.games[8].fellowPlayers[1].summonerId;
          this.match8_player2Id = this.recentGames.games[8].fellowPlayers[2].summonerId;
          this.match8_player3Id = this.recentGames.games[8].fellowPlayers[3].summonerId;
          this.match8_player4Id = this.recentGames.games[8].fellowPlayers[4].summonerId;
          this.match8_player5Id = this.recentGames.games[8].fellowPlayers[5].summonerId;
          this.match8_player6Id = this.recentGames.games[8].fellowPlayers[6].summonerId;
          this.match8_player7Id = this.recentGames.games[8].fellowPlayers[7].summonerId;
          this.match8_player8Id = this.recentGames.games[8].fellowPlayers[8].summonerId;

          this.match9_player0Id = this.recentGames.games[9].fellowPlayers[0].summonerId;
          this.match9_player1Id = this.recentGames.games[9].fellowPlayers[1].summonerId;
          this.match9_player2Id = this.recentGames.games[9].fellowPlayers[2].summonerId;
          this.match9_player3Id = this.recentGames.games[9].fellowPlayers[3].summonerId;
          this.match9_player4Id = this.recentGames.games[9].fellowPlayers[4].summonerId;
          this.match9_player5Id = this.recentGames.games[9].fellowPlayers[5].summonerId;
          this.match9_player6Id = this.recentGames.games[9].fellowPlayers[6].summonerId;
          this.match9_player7Id = this.recentGames.games[9].fellowPlayers[7].summonerId;
          this.match9_player8Id = this.recentGames.games[9].fellowPlayers[8].summonerId;

          this.findChampName0(this.champId_0);
          this.findChampName1(this.champId_1);
          this.findChampName2(this.champId_2);
          this.findChampName3(this.champId_3);
          this.findChampName4(this.champId_4);
          this.findChampName5(this.champId_5);
          this.findChampName6(this.champId_6);
          this.findChampName7(this.champId_7);
          this.findChampName8(this.champId_8);
          this.findChampName9(this.champId_9);

          this.setItemText0_0(this.item0_0);
          this.setItemText1_0(this.item1_0);
          this.setItemText2_0(this.item2_0);
          this.setItemText3_0(this.item3_0);
          this.setItemText4_0(this.item4_0);
          this.setItemText5_0(this.item5_0);
          this.setItemText6_0(this.item6_0);

          this.setItemText0_1(this.item0_1);
          this.setItemText1_1(this.item1_1);
          this.setItemText2_1(this.item2_1);
          this.setItemText3_1(this.item3_1);
          this.setItemText4_1(this.item4_1);
          this.setItemText5_1(this.item5_1);
          this.setItemText6_1(this.item6_1);

          this.setItemText0_2(this.item0_2);
          this.setItemText1_2(this.item1_2);
          this.setItemText2_2(this.item2_2);
          this.setItemText3_2(this.item3_2);
          this.setItemText4_2(this.item4_2);
          this.setItemText5_2(this.item5_2);
          this.setItemText6_2(this.item6_2);

          this.setItemText0_3(this.item0_3);
          this.setItemText1_3(this.item1_3);
          this.setItemText2_3(this.item2_3);
          this.setItemText3_3(this.item3_3);
          this.setItemText4_3(this.item4_3);
          this.setItemText5_3(this.item5_3);
          this.setItemText6_3(this.item6_3);

          this.setItemText0_4(this.item0_4);
          this.setItemText1_4(this.item1_4);
          this.setItemText2_4(this.item2_4);
          this.setItemText3_4(this.item3_4);
          this.setItemText4_4(this.item4_4);
          this.setItemText5_4(this.item5_4);
          this.setItemText6_4(this.item6_4);

          this.setItemText0_5(this.item0_5);
          this.setItemText1_5(this.item1_5);
          this.setItemText2_5(this.item2_5);
          this.setItemText3_5(this.item3_5);
          this.setItemText4_5(this.item4_5);
          this.setItemText5_5(this.item5_5);
          this.setItemText6_5(this.item6_5);

          this.setItemText0_6(this.item0_6);
          this.setItemText1_6(this.item1_6);
          this.setItemText2_6(this.item2_6);
          this.setItemText3_6(this.item3_6);
          this.setItemText4_6(this.item4_6);
          this.setItemText5_6(this.item5_6);
          this.setItemText6_6(this.item6_6);

          this.setItemText0_7(this.item0_7);
          this.setItemText1_7(this.item1_7);
          this.setItemText2_7(this.item2_7);
          this.setItemText3_7(this.item3_7);
          this.setItemText4_7(this.item4_7);
          this.setItemText5_7(this.item5_7);
          this.setItemText6_7(this.item6_7);

          this.setItemText0_8(this.item0_8);
          this.setItemText1_8(this.item1_8);
          this.setItemText2_8(this.item2_8);
          this.setItemText3_8(this.item3_8);
          this.setItemText4_8(this.item4_8);
          this.setItemText5_8(this.item5_8);
          this.setItemText6_8(this.item6_8);

          this.setItemText0_9(this.item0_9);
          this.setItemText1_9(this.item1_9);
          this.setItemText2_9(this.item2_9);
          this.setItemText3_9(this.item3_9);
          this.setItemText4_9(this.item4_9);
          this.setItemText5_9(this.item5_9);
          this.setItemText6_9(this.item6_9);
          

         
          
      })
      this.Searched = true;
  }
//DON'T JUDGE ME, I COULD NOT GET THIS TO DYNAMICALLY SAVE TO THE DIFFERENT NAMES IN ONE METHOD
//I KNOW IT'S UGLY!!! D: 
    isValid(itemId:number){
        if(itemId == 0|| itemId == undefined){
           return true;
        }
        else{
            return false;
        }
    }

    isChampValid(champId:number)
    {
        if(champId == 0 || champId == undefined)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
  findChampName0(champId: number)
  {
      this.summonerService.getChampName(champId)
        .subscribe(response => {
            this.champ = response;
            this.champName_0 = this.champ.image.full;
            this.cName0 = this.champ.name;
        })
  }
  findChampName1(champId: number)
  {
      this.summonerService.getChampName(champId)
        .subscribe(response => {
            this.champ = response;
            this.champName_1 = this.champ.image.full;
            this.cName1 = this.champ.name;
        })
  }
  findChampName2(champId: number)
  {
      this.summonerService.getChampName(champId)
        .subscribe(response => {
            this.champ = response;
            this.champName_2 = this.champ.image.full;
            this.cName2 = this.champ.name;
        })
  }
  findChampName3(champId: number)
  {
      this.summonerService.getChampName(champId)
        .subscribe(response => {
            this.champ = response;
            this.champName_3 = this.champ.image.full;
            this.cName3 = this.champ.name;
        })
  }
  findChampName4(champId: number)
  {
      this.summonerService.getChampName(champId)
        .subscribe(response => {
            this.champ = response;
            this.champName_4 = this.champ.image.full;
            this.cName4 = this.champ.name;
        })
  }
  findChampName5(champId: number)
  {
      this.summonerService.getChampName(champId)
        .subscribe(response => {
            this.champ = response;
            this.champName_5 = this.champ.image.full;
            this.cName5 = this.champ.name;
        })
  }
  findChampName6(champId: number)
  {
      this.summonerService.getChampName(champId)
        .subscribe(response => {
            this.champ = response;
            this.champName_6 = this.champ.image.full;
            this.cName6 = this.champ.name;
        })
  }
  findChampName7(champId: number)
  {
      this.summonerService.getChampName(champId)
        .subscribe(response => {
            this.champ = response;
            this.champName_7 = this.champ.image.full;
            this.cName7 = this.champ.name;
        })
  }
  findChampName8(champId: number)
  {
      this.summonerService.getChampName(champId)
        .subscribe(response => {
            this.champ = response;
            this.champName_8 = this.champ.image.full;
            this.cName8 = this.champ.name;
        })
  }
  findChampName9(champId: number)
  {
      this.summonerService.getChampName(champId)
        .subscribe(response => {
            this.champ = response;
            this.champName_9 = this.champ.image.full;
            this.cName9 = this.champ.name;
        })
  }
  
  
setItemText0_0(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item0_0text = this.item.name;
        })
        
  }
  setItemText1_0(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item1_0text = this.item.name;
        })
       
  }
  setItemText2_0(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item2_0text = this.item.name;
        })

  }
  setItemText3_0(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item3_0text = this.item.name;
        })
  }
  setItemText4_0(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item4_0text = this.item.name;
        })
  }
  setItemText5_0(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item5_0text = this.item.name;
        })
  }
  setItemText6_0(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item6_0text = this.item.name;
        })
  }


  setItemText0_1(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item0_1text = this.item.name;
        })
  }
  setItemText1_1(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item1_1text = this.item.name;
        })
  }
  setItemText2_1(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item2_1text = this.item.name;
        })
  }
  setItemText3_1(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item3_1text = this.item.name;
        })
  }
  setItemText4_1(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item4_1text = this.item.name;
        })
  }
  setItemText5_1(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item5_1text = this.item.name;
        })
  }
  setItemText6_1(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item6_1text = this.item.name;
        })
  }
  setItemText0_2(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item0_2text = this.item.name;
        })
  }
  setItemText1_2(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item1_2text = this.item.name;
        })
  }
  setItemText2_2(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item2_2text = this.item.name;
        })
  }
  setItemText3_2(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item3_2text = this.item.name;
        })
  }
  setItemText4_2(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item4_2text = this.item.name;
        })
  }
  setItemText5_2(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item5_2text = this.item.name;
        })
        
  }
  setItemText6_2(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item6_2text = this.item.name;
        })
       
  }
  setItemText0_3(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item0_3text = this.item.name;
        })
  }
  setItemText1_3(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item1_3text = this.item.name;
        })
  }
  setItemText2_3(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item2_3text = this.item.name;
        })
  }
  setItemText3_3(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item3_3text = this.item.name;
        })
  }
  setItemText4_3(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item4_3text = this.item.name;
        })
  }
  setItemText5_3(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item5_3text = this.item.name;
        })
  }
  setItemText6_3(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item6_3text = this.item.name;
        })
       
  }
  setItemText0_4(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item0_4text = this.item.name;
        })
  }
  setItemText1_4(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item1_4text = this.item.name;
        })
  }
  setItemText2_4(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item2_4text = this.item.name;
        })
  }
  setItemText3_4(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item3_4text = this.item.name;
        })
  }
  setItemText4_4(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item4_4text = this.item.name;
        })
  }
  setItemText5_4(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item5_4text = this.item.name;
        })
  }
  setItemText6_4(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item6_4text = this.item.name;
        })
  }
  setItemText0_5(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item0_5text = this.item.name;
        })
  }
  setItemText1_5(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item1_5text = this.item.name;
        })
  }
  setItemText2_5(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item2_5text = this.item.name;
        })
  }
  setItemText3_5(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item3_5text = this.item.name;
        })
  }
  setItemText4_5(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item4_5text = this.item.name;
        })
  }
  setItemText5_5(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item5_5text = this.item.name;
        })
  }
  setItemText6_5(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item6_5text = this.item.name;
        })
  }
  setItemText0_6(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item0_6text = this.item.name;
        })
  }
  setItemText1_6(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item1_6text = this.item.name;
        })
  }
  setItemText2_6(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item2_6text = this.item.name;
        })
  }
  setItemText3_6(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item3_6text = this.item.name;
        })
       
  }
  setItemText4_6(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item4_6text = this.item.name;
        })
  }
  setItemText5_6(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item5_6text = this.item.name;
        })
  }
  setItemText6_6(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item6_6text = this.item.name;
        })
  }
  setItemText0_7(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item0_7text = this.item.name;
        })
  }
  setItemText1_7(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item1_7text = this.item.name;
        })
  }
  setItemText2_7(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item2_7text = this.item.name;
        })
  }
  setItemText3_7(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item3_7text = this.item.name;
        })
  }
  setItemText4_7(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item4_7text = this.item.name;
        })
  }
  setItemText5_7(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item5_7text = this.item.name;
        })
  }
  setItemText6_7(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item6_7text = this.item.name;
        })
  }
  setItemText0_8(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item0_8text = this.item.name;
        })
  }
  setItemText1_8(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item1_8text = this.item.name;
        })
  }
  setItemText2_8(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item2_8text = this.item.name;
        })
  }
  setItemText3_8(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item3_8text = this.item.name;
        })
  }
  setItemText4_8(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item4_8text = this.item.name;
        })
  }
  setItemText5_8(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item5_8text = this.item.name;
        })
  }
  setItemText6_8(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item6_8text = this.item.name;
        })
  }
  setItemText0_9(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item0_9text = this.item.name;
        })
  }
  setItemText1_9(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item1_9text = this.item.name;
        })
  }
  setItemText2_9(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item2_9text = this.item.name;
        })
  }
  setItemText3_9(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item3_9text = this.item.name;
        })
  }
  setItemText4_9(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item4_9text = this.item.name;
        })
  }
  setItemText5_9(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item5_9text = this.item.name;
        })
  }
  setItemText6_9(itemId: number)
  {
      this.summonerService.getItemName(itemId)
        .subscribe(response => {
            this.item = response;
            this.item6_9text = this.item.name;
        })
  }

  moreInfo0()
  {
      if(this.MoreInfo0 == true)
      {
          this.MoreInfo0 = false;
      }
      else
      {
          //let playerIds: number[];
          this.MoreInfo0 = true;
          var summonerNames:string[]; 
          summonerNames = ['','','','','','','','',''];
          this.summonerService.getSummonerById(this.match0_player0Id, this.match0_player1Id, this.match0_player2Id, this.match0_player3Id, this.match0_player4Id, this.match0_player5Id, this.match0_player6Id, this.match0_player7Id, this.match0_player8Id)
            .subscribe(response => {
                this.summonerNameData = response;
                let t;
                let i = 0;
                for(var prop in this.summonerNameData)
                {
                    if(i == 0)
                    {
                        this.match0_player0Name = this.summonerNameData[prop];
                    }
                    else if(i == 1)
                    {
                        this.match0_player1Name = this.summonerNameData[prop];
                    }
                    else if(i == 2)
                    {
                        this.match0_player2Name = this.summonerNameData[prop];
                    }
                    else if(i == 3)
                    {
                        this.match0_player3Name = this.summonerNameData[prop];
                    }
                    else if(i == 4)
                    {
                        this.match0_player4Name = this.summonerNameData[prop];
                    }
                    else if(i == 5)
                    {
                        this.match0_player5Name = this.summonerNameData[prop];
                    }
                    else if(i == 6)
                    {
                        this.match0_player6Name = this.summonerNameData[prop];
                    }
                    else if(i == 7)
                    {
                        this.match0_player7Name = this.summonerNameData[prop];
                    }
                    else if(i == 8)
                    {
                        this.match0_player8Name = this.summonerNameData[prop];
                    }
                    i++;
                }
            })
      }
      
  }

  moreInfo1()
  {
      if(this.MoreInfo1 == true)
      {
          this.MoreInfo1 = false;
      }
      else
      {
            this.MoreInfo1 = true;
            var summonerNames:string[]; 
            summonerNames = ['','','','','','','','',''];
            this.summonerService.getSummonerById(this.match1_player0Id, this.match1_player1Id, this.match1_player2Id, this.match1_player3Id, this.match1_player4Id, this.match1_player5Id, this.match1_player6Id, this.match1_player7Id, this.match1_player8Id)
            .subscribe(response => {
                this.summonerNameData = response;
                let t;
                let i = 0;
                for(var prop in this.summonerNameData)
                {
                    if(i == 0)
                    {
                        this.match1_player0Name = this.summonerNameData[prop];
                    }
                    else if(i == 1)
                    {
                        this.match1_player1Name = this.summonerNameData[prop];
                    }
                    else if(i == 2)
                    {
                        this.match1_player2Name = this.summonerNameData[prop];
                    }
                    else if(i == 3)
                    {
                        this.match1_player3Name = this.summonerNameData[prop];
                    }
                    else if(i == 4)
                    {
                        this.match1_player4Name = this.summonerNameData[prop];
                    }
                    else if(i == 5)
                    {
                        this.match1_player5Name = this.summonerNameData[prop];
                    }
                    else if(i == 6)
                    {
                        this.match1_player6Name = this.summonerNameData[prop];
                    }
                    else if(i == 7)
                    {
                        this.match1_player7Name = this.summonerNameData[prop];
                    }
                    else if(i == 8)
                    {
                        this.match1_player8Name = this.summonerNameData[prop];
                    }
                    i++;
                }
            })
      }
  }

  moreInfo2()
  {
      if(this.MoreInfo2 == true)
      {
          this.MoreInfo2 = false;
      }
      else
      {
            this.MoreInfo2 = true;
            var summonerNames:string[]; 
            summonerNames = ['','','','','','','','',''];
            this.summonerService.getSummonerById(this.match2_player0Id, this.match2_player1Id, this.match2_player2Id, this.match2_player3Id, this.match2_player4Id, this.match2_player5Id, this.match2_player6Id, this.match2_player7Id, this.match2_player8Id)
            .subscribe(response => {
                this.summonerNameData = response;
                let t;
                let i = 0;
                for(var prop in this.summonerNameData)
                {
                    if(i == 0)
                    {
                        this.match2_player0Name = this.summonerNameData[prop];
                    }
                    else if(i == 1)
                    {
                        this.match2_player1Name = this.summonerNameData[prop];
                    }
                    else if(i == 2)
                    {
                        this.match2_player2Name = this.summonerNameData[prop];
                    }
                    else if(i == 3)
                    {
                        this.match2_player3Name = this.summonerNameData[prop];
                    }
                    else if(i == 4)
                    {
                        this.match2_player4Name = this.summonerNameData[prop];
                    }
                    else if(i == 5)
                    {
                        this.match2_player5Name = this.summonerNameData[prop];
                    }
                    else if(i == 6)
                    {
                        this.match2_player6Name = this.summonerNameData[prop];
                    }
                    else if(i == 7)
                    {
                        this.match2_player7Name = this.summonerNameData[prop];
                    }
                    else if(i == 8)
                    {
                        this.match2_player8Name = this.summonerNameData[prop];
                    }
                    i++;
                }
            })
      }
  }

  moreInfo3()
  {
      if(this.MoreInfo3 == true)
      {
          this.MoreInfo3 = false;
      }
      else
      {
            this.MoreInfo3 = true;
            var summonerNames:string[]; 
            summonerNames = ['','','','','','','','',''];
            this.summonerService.getSummonerById(this.match3_player0Id, this.match3_player1Id, this.match3_player2Id, this.match3_player3Id, this.match3_player4Id, this.match3_player5Id, this.match3_player6Id, this.match3_player7Id, this.match3_player8Id)
            .subscribe(response => {
                this.summonerNameData = response;
                let t;
                let i = 0;
                for(var prop in this.summonerNameData)
                {
                    if(i == 0)
                    {
                        this.match3_player0Name = this.summonerNameData[prop];
                    }
                    else if(i == 1)
                    {
                        this.match3_player1Name = this.summonerNameData[prop];
                    }
                    else if(i == 2)
                    {
                        this.match3_player2Name = this.summonerNameData[prop];
                    }
                    else if(i == 3)
                    {
                        this.match3_player3Name = this.summonerNameData[prop];
                    }
                    else if(i == 4)
                    {
                        this.match3_player4Name = this.summonerNameData[prop];
                    }
                    else if(i == 5)
                    {
                        this.match3_player5Name = this.summonerNameData[prop];
                    }
                    else if(i == 6)
                    {
                        this.match3_player6Name = this.summonerNameData[prop];
                    }
                    else if(i == 7)
                    {
                        this.match3_player7Name = this.summonerNameData[prop];
                    }
                    else if(i == 8)
                    {
                        this.match3_player8Name = this.summonerNameData[prop];
                    }
                    i++;
                }
            })
      }
  }

  moreInfo4()
  {
      if(this.MoreInfo4 == true)
      {
          this.MoreInfo4 = false;
      }
      else
      {
            this.MoreInfo4 = true;
            var summonerNames:string[]; 
            summonerNames = ['','','','','','','','',''];
            this.summonerService.getSummonerById(this.match4_player0Id, this.match4_player1Id, this.match4_player2Id, this.match4_player3Id, this.match4_player4Id, this.match4_player5Id, this.match4_player6Id, this.match4_player7Id, this.match4_player8Id)
            .subscribe(response => {
                this.summonerNameData = response;
                let t;
                let i = 0;
                for(var prop in this.summonerNameData)
                {
                    if(i == 0)
                    {
                        this.match5_player0Name = this.summonerNameData[prop];
                    }
                    else if(i == 1)
                    {
                        this.match4_player1Name = this.summonerNameData[prop];
                    }
                    else if(i == 2)
                    {
                        this.match4_player2Name = this.summonerNameData[prop];
                    }
                    else if(i == 3)
                    {
                        this.match4_player3Name = this.summonerNameData[prop];
                    }
                    else if(i == 4)
                    {
                        this.match4_player4Name = this.summonerNameData[prop];
                    }
                    else if(i == 5)
                    {
                        this.match4_player5Name = this.summonerNameData[prop];
                    }
                    else if(i == 6)
                    {
                        this.match4_player6Name = this.summonerNameData[prop];
                    }
                    else if(i == 7)
                    {
                        this.match4_player7Name = this.summonerNameData[prop];
                    }
                    else if(i == 8)
                    {
                        this.match4_player8Name = this.summonerNameData[prop];
                    }
                    i++;
                }
            })
      }
  }

  moreInfo5()
  {
      if(this.MoreInfo5 == true)
      {
          this.MoreInfo5 = false;
      }
      else
      {
            this.MoreInfo5 = true;
            var summonerNames:string[]; 
            summonerNames = ['','','','','','','','',''];
            this.summonerService.getSummonerById(this.match5_player0Id, this.match5_player1Id, this.match5_player2Id, this.match5_player3Id, this.match5_player4Id, this.match5_player5Id, this.match5_player6Id, this.match5_player7Id, this.match5_player8Id)
            .subscribe(response => {
                this.summonerNameData = response;
                let t;
                let i = 0;
                for(var prop in this.summonerNameData)
                {
                    if(i == 0)
                    {
                        this.match5_player0Name = this.summonerNameData[prop];
                    }
                    else if(i == 1)
                    {
                        this.match5_player1Name = this.summonerNameData[prop];
                    }
                    else if(i == 2)
                    {
                        this.match5_player2Name = this.summonerNameData[prop];
                    }
                    else if(i == 3)
                    {
                        this.match5_player3Name = this.summonerNameData[prop];
                    }
                    else if(i == 4)
                    {
                        this.match5_player4Name = this.summonerNameData[prop];
                    }
                    else if(i == 5)
                    {
                        this.match5_player5Name = this.summonerNameData[prop];
                    }
                    else if(i == 6)
                    {
                        this.match5_player6Name = this.summonerNameData[prop];
                    }
                    else if(i == 7)
                    {
                        this.match5_player7Name = this.summonerNameData[prop];
                    }
                    else if(i == 8)
                    {
                        this.match5_player8Name = this.summonerNameData[prop];
                    }
                    i++;
                }
            })
      }
  }

  moreInfo6()
  {
      if(this.MoreInfo6 == true)
      {
          this.MoreInfo6 = false;
      }
      else
      {
            this.MoreInfo6 = true;
            var summonerNames:string[]; 
            summonerNames = ['','','','','','','','',''];
            this.summonerService.getSummonerById(this.match6_player0Id, this.match6_player1Id, this.match6_player2Id, this.match6_player3Id, this.match6_player4Id, this.match6_player5Id, this.match6_player6Id, this.match6_player7Id, this.match6_player8Id)
            .subscribe(response => {
                this.summonerNameData = response;
                let t;
                let i = 0;
                for(var prop in this.summonerNameData)
                {
                    if(i == 0)
                    {
                        this.match6_player0Name = this.summonerNameData[prop];
                    }
                    else if(i == 1)
                    {
                        this.match6_player1Name = this.summonerNameData[prop];
                    }
                    else if(i == 2)
                    {
                        this.match6_player2Name = this.summonerNameData[prop];
                    }
                    else if(i == 3)
                    {
                        this.match6_player3Name = this.summonerNameData[prop];
                    }
                    else if(i == 4)
                    {
                        this.match6_player4Name = this.summonerNameData[prop];
                    }
                    else if(i == 5)
                    {
                        this.match6_player5Name = this.summonerNameData[prop];
                    }
                    else if(i == 6)
                    {
                        this.match6_player6Name = this.summonerNameData[prop];
                    }
                    else if(i == 7)
                    {
                        this.match6_player7Name = this.summonerNameData[prop];
                    }
                    else if(i == 8)
                    {
                        this.match6_player8Name = this.summonerNameData[prop];
                    }
                    i++;
                }
            })
      }
  }

  moreInfo7()
  {
      if(this.MoreInfo7 == true)
      {
          this.MoreInfo7 = false;
      }
      else
      {
            this.MoreInfo7 = true;
            var summonerNames:string[]; 
            summonerNames = ['','','','','','','','',''];
            this.summonerService.getSummonerById(this.match7_player0Id, this.match7_player1Id, this.match7_player2Id, this.match7_player3Id, this.match7_player4Id, this.match7_player5Id, this.match7_player6Id, this.match7_player7Id, this.match7_player8Id)
            .subscribe(response => {
                this.summonerNameData = response;
                let t;
                let i = 0;
                for(var prop in this.summonerNameData)
                {
                    if(i == 0)
                    {
                        this.match7_player0Name = this.summonerNameData[prop];
                    }
                    else if(i == 1)
                    {
                        this.match7_player1Name = this.summonerNameData[prop];
                    }
                    else if(i == 2)
                    {
                        this.match7_player2Name = this.summonerNameData[prop];
                    }
                    else if(i == 3)
                    {
                        this.match7_player3Name = this.summonerNameData[prop];
                    }
                    else if(i == 4)
                    {
                        this.match7_player4Name = this.summonerNameData[prop];
                    }
                    else if(i == 5)
                    {
                        this.match7_player5Name = this.summonerNameData[prop];
                    }
                    else if(i == 6)
                    {
                        this.match7_player6Name = this.summonerNameData[prop];
                    }
                    else if(i == 7)
                    {
                        this.match7_player7Name = this.summonerNameData[prop];
                    }
                    else if(i == 8)
                    {
                        this.match7_player8Name = this.summonerNameData[prop];
                    }
                    i++;
                }
            })
      }
  }

  moreInfo8()
  {
      if(this.MoreInfo8 == true)
      {
          this.MoreInfo8 = false;
      }
      else
      {
            this.MoreInfo8 = true;
            var summonerNames:string[]; 
            summonerNames = ['','','','','','','','',''];
            this.summonerService.getSummonerById(this.match8_player0Id, this.match8_player1Id, this.match8_player2Id, this.match8_player3Id, this.match8_player4Id, this.match8_player5Id, this.match8_player6Id, this.match8_player7Id, this.match8_player8Id)
            .subscribe(response => {
                this.summonerNameData = response;
                let t;
                let i = 0;
                for(var prop in this.summonerNameData)
                {
                    if(i == 0)
                    {
                        this.match8_player0Name = this.summonerNameData[prop];
                    }
                    else if(i == 1)
                    {
                        this.match8_player1Name = this.summonerNameData[prop];
                    }
                    else if(i == 2)
                    {
                        this.match8_player2Name = this.summonerNameData[prop];
                    }
                    else if(i == 3)
                    {
                        this.match8_player3Name = this.summonerNameData[prop];
                    }
                    else if(i == 4)
                    {
                        this.match8_player4Name = this.summonerNameData[prop];
                    }
                    else if(i == 5)
                    {
                        this.match8_player5Name = this.summonerNameData[prop];
                    }
                    else if(i == 6)
                    {
                        this.match8_player6Name = this.summonerNameData[prop];
                    }
                    else if(i == 7)
                    {
                        this.match8_player7Name = this.summonerNameData[prop];
                    }
                    else if(i == 8)
                    {
                        this.match8_player8Name = this.summonerNameData[prop];
                    }
                    i++;
                }
            })
      }
  }

  moreInfo9()
  {
      if(this.MoreInfo9 == true)
      {
          this.MoreInfo9 = false;
      }
      else
      {
            this.MoreInfo9 = true;
            var summonerNames:string[]; 
            summonerNames = ['','','','','','','','',''];
            this.summonerService.getSummonerById(this.match9_player0Id, this.match9_player1Id, this.match9_player2Id, this.match9_player3Id, this.match9_player4Id, this.match9_player5Id, this.match9_player6Id, this.match9_player7Id, this.match9_player8Id)
            .subscribe(response => {
                this.summonerNameData = response;
                let t;
                let i = 0;
                for(var prop in this.summonerNameData)
                {
                    if(i == 0)
                    {
                        this.match9_player0Name = this.summonerNameData[prop];
                    }
                    else if(i == 1)
                    {
                        this.match9_player1Name = this.summonerNameData[prop];
                    }
                    else if(i == 2)
                    {
                        this.match9_player2Name = this.summonerNameData[prop];
                    }
                    else if(i == 3)
                    {
                        this.match9_player3Name = this.summonerNameData[prop];
                    }
                    else if(i == 4)
                    {
                        this.match9_player4Name = this.summonerNameData[prop];
                    }
                    else if(i == 5)
                    {
                        this.match9_player5Name = this.summonerNameData[prop];
                    }
                    else if(i == 6)
                    {
                        this.match9_player6Name = this.summonerNameData[prop];
                    }
                    else if(i == 7)
                    {
                        this.match9_player7Name = this.summonerNameData[prop];
                    }
                    else if(i == 8)
                    {
                        this.match9_player8Name = this.summonerNameData[prop];
                    }
                    i++;
                }
            })
      }
  }

} 
