import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import { Player } from './player';
import { RecentGames } from './recentGames';
import { MatchHistory } from './matchHistory';
import { MatchList } from './matchList';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

@Injectable()
export class SummonerService
{
    private baseUrl: string = 'https://na.api.pvp.net/api/lol/na';
    private userName: string;
    private apiKey: string = 'RGAPI-51d6076a-27c8-4dca-a698-50ef9cf2aeb8';
    constructor(private http: Http) { }

    getPlayer(name:string)
    {
            return this.http.get(`${this.baseUrl}/v1.4/summoner/by-name/${name}/?api_key=${this.apiKey}`).map((response) => response.json()).catch(this.handleError);
    }

    getGamesList(summonerId: number)
    {
        return this.http.get(`${this.baseUrl}/v1.3/game/by-summoner/${summonerId}/recent?api_key=${this.apiKey}`).map((response) => response.json());
    }

    getChampName(champId: number)
    {
        return this.http.get(`https://global.api.pvp.net/api/lol/static-data/na/v1.2/champion/${champId}?champData=image&api_key=${this.apiKey}`).map((response) => response.json());
    }

    getItemName(itemId: number)
    {
        if(itemId !=0){
            return this.http.get(`https://global.api.pvp.net/api/lol/static-data/na/v1.2/item/${itemId}?api_key=${this.apiKey}`).map((response) => response.json());
        }
    }

    getSummonerById(summonerId0: number, summonerId1: number, summonerId2: number, summonerId3: number, summonerId4: number, summonerId5: number, summonerId6: number, summonerId7: number, summonerId8: number)
    {
        return this.http.get(`${this.baseUrl}/v1.4/summoner/${summonerId0},${summonerId1},${summonerId2},${summonerId3},${summonerId4},${summonerId5},${summonerId6},${summonerId7},${summonerId8}/name/?api_key=${this.apiKey}`)
            .map((response) => response.json());
    }


    getMatchList(summonerId: number)
    {
        return this.http.get(`${this.baseUrl}/v2.2/matchlist/by-summoner/${summonerId}?api_key=${this.apiKey}`).map((response) => response.json());
    }

    getMatch(matchId: number)
    {
        return this.http.get(`${this.baseUrl}v2.2/match/${matchId}?api_key=${this.apiKey}`).map((response) => response.json());
    }
            

    handleError(error: Response | any){
        //let errmsg: string;
        //if (error instanceof Response)
        alert(`That username doesn't exist!`);
        return Observable.throw("Invalid name");
    }
}